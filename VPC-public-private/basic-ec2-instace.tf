/* provider "aws" {
    access_key = "AKIAIBY4IZ4PSWCMYTMQ"
    secret_key = "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"
    region = "us-west-2"
} */

provider "aws"{
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secreet_key}"
    region = "ap-south-1"
}
# Create VPC
resource "aws_vpc" "terraformtestvpc" {
    cidr_block = "10.0.0.0/16"
    tags {
        Name = "terrorformtestvpc"
    }
}
# create Internet gateway

resource "aws_internet_gateway" "terrafromIG" {
    vpc_id = "${aws_vpc.terraformtestvpc.id}"
    tags {
        Name = "terraformIG"
    }
}

#create subnet - public

resource "aws_subnet" "public1" {
    vpc_id = "${aws_vpc.terraformtestvpc.id}"
    cidr_block = "10.0.1.0/24"
    availability_zone = "ap-south-1a"
    tags {
        Name = "public1"
    }
}

#create subnet - private

resource "aws_subnet" "private1" {
    vpc_id = "${aws_vpc.terraformtestvpc.id}"
    cidr_block = "10.0.2.0/24"
    availability_zone = "ap-south-1b"
    tags {
        Name = "private1"
    }
}

#New routing table

resource "aws_route_table" "public1RT" {
    vpc_id = "${aws_vpc.terraformtestvpc.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.terrafromIG.id}"
    }
    tags {
        Name = "public1RT"
    }
}
# subnet assocation

resource "aws_route_table_association" "subnetassosicate" {
    subnet_id = "${aws_subnet.public1.id}"
    route_table_id = "${aws_route_table.public1RT.id}"
}


#create security group

resource "aws_security_group" "internal" {
    name = "accesshttp"
    vpc_id = "${aws_vpc.terraformtestvpc.id}"
    ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
    }
    tags {
        Name = "accesshttp"
    }
    
}


# create instance in private subnet
resource "aws_instance" "Privatetest1" {
    ami = "ami-d7abd1b8"
    instance_type = "t2.micro"
    key_name = "cf"
    vpc_security_group_ids = ["${aws_security_group.internal.id}"]
    subnet_id = "${aws_subnet.private1.id}"
    tags {
        Name = "Privatetest1"
         PG = "linuxPG"
    }

} 