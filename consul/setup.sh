#!/bin/bash

confdir="/home/ec2-user"
conffile="config-server.json"
servicefile="service.sh"

yum -y install wget
yum -y install unzip
# source function library
. /etc/rc.d/init.d/functions


#Add /usr/local/bin to PATH in profile
echo 'export PATH=$PATH:/usr/local/bin' >> /etc/profile

source /etc/profile
cd /usr/local/bin
wget https://releases.hashicorp.com/consul/1.0.6/consul_1.0.6_linux_amd64.zip
unzip *.zip
rm -f *.zip
mkdir -p /etc/consul.d
mkdir -p /data/consul
mkdir -p /logs/consul
cp $confdir/$conffile /etc/consul.d/config.json -f
#Security - Allow Only root user to run or access the consul files and Apps.
chown root:root /etc/consul.d
chmod 700 /etc/consul.d
chown root:root /data/consul
chmod 700 /data/consul
cp $confdir/$servicefile /etc/init.d/consul -f
chown root:root /etc/init.d/consul
chmod 700 /etc/init.d/consul
#Service Setup: #in centOS & RedHat
#touch /etc/init.d/consul
#past the consul-service.ps1 script
chmod +x /etc/init.d/consul
#service consul start
cd /usr/bin
ln -s /usr/local/bin/consul consul
