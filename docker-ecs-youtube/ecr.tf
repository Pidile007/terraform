provider "aws"{
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secreet_key}"
    region = "ap-southeast-1"
}

resource "aws_ecr_repository" "myapp" {
    name = "myapp"
}